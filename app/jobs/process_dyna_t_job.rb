require 'cache_handler'

class ProcessDynaTJob < ApplicationJob
  def perform(struct)
    @root_id = struct['id']
    normalized_tree = process(@root_id, struct['child'])
    cache_handler.update(normalized_tree)
  end

  private

  def cache_handler
    CacheHandler.instance
  end

  def process(cur_id, children = [], norma = {})
    norma = update_parent_of_children(norma, cur_id, children)
    norma = update_cur_children(norma, cur_id, children)
    norma = update_childrens_up(norma, cur_id, children)
    children.each do |child|
      norma = process(child['id'], child['child'], norma)
    end
    norma
  end

  def update_parent_of_children(norma, cur_id, children)
    cur_norma = get_norma_node(norma, cur_id)
    cur_parents = cur_norma['parents']
    children.each do |child|
      child_norma = get_norma_node(norma, child['id'])
      child_norma['parents'].append(cur_id)
      unless cur_parents.empty?
        child_norma['parents'].append(*cur_parents)
      end
      child_norma['parents'].uniq!
      norma[child['id']] = child_norma
    end
    norma
  end

  def update_cur_children(norma, cur_id, children)
    cur_norma = get_norma_node(norma, cur_id)
    cur_norma['children'] = children
    norma[cur_id] = cur_norma
    norma
  end

  def update_childrens_up(norma, cur_id, children)
    cur_norma = get_norma_node(norma, cur_id)
    cur_parents = cur_norma['parents']
    cur_parents.each do |p|
      parent_node = get_norma_node(norma, p)
      unless children.empty?
        parent_node['children'].append(*children)
        parent_node['children'].uniq! { |c| c['id'] }
      end
      norma[p] = parent_node
    end
    norma
  end

  def get_norma_node(norma, id)
    norma[id] || empty_node
  end

  def empty_node
    {'parents' => [], 'children' => []}
  end
end
