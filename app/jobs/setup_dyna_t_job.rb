class SetupDynaTJob < ApplicationJob
  def perform
    fetcher.fetch!
    ProcessDynaTJob.perform_now(fetcher.data)
  end

  private

  def fetcher
    Fetcher.instance
  end
end
