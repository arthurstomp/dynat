require 'persister'
require 'cache_handler'

class DynaTree
  def self.tree(id)
    tree = original_tree
    return nil unless tree && tree['id'].to_s == id
    original_tree
  end

  def self.parents(tree_id, id)
    tree = original_tree
    return nil unless tree && tree['id'].to_s == tree_id
    node = cache_handler.get(id)
    if node
      node['parents']
    else
      []
    end
  end

  def self.children(tree_id, id)
    tree = original_tree
    return nil unless tree && tree['id'].to_s == tree_id
    node = cache_handler.get(id)
    if node
      node['children']
    else
      []
    end
  end

  private

  def self.original_tree
    cache_handler.original
  end

  def self.cache_handler
    CacheHandler.instance
  end
end
