class DynaTController < ApplicationController
  def cached_dyna_t
    render json: DynaTree.tree(params[:tree_id])
  end

  def parent
    parents = DynaTree.parents(params[:tree_id], params[:id])
    render json: parents
  end

  def child
    children = DynaTree.children(params[:tree_id], params[:id])
    render json: children
  end
end
