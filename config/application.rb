require_relative 'boot'

require "rails"
# Pick the frameworks you want:
require "active_model/railtie"
require "active_job/railtie"
# require "active_record/railtie"
# require "active_storage/engine"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "action_cable/engine"
# require "sprockets/railtie"
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module DynaT
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    # Only loads a smaller set of middleware suitable for API only apps.
    # Middleware like session, flash, cookies can be added back manually.
    # Skip views, helpers and assets when generating a new resource.
    config.api_only = true

    # Autoload lib files
    config.autoload_paths << Rails.root.join('lib')

    # Config Active Job
    config.active_job.queue_adapter = :sidekiq

    # Configure persistence of dynamic tree.
    config.dyna_t_dir = Rails.root.join('tmp', 'dyna_t')
    config.dyna_t_filename = 'dyna_t.json'
    config.dyna_t_source = ENV['DYNA_T_SOURCE'] || "https://random-tree.herokuapp.com/"
    # Lambda that perform the persistence of a dynamic tree
    config.dyna_t_persist_worker = nil
    # Lambda that perform the fetch of a dynamic tree
    config.dyna_t_fetch_worker = nil

    # Configure Redis
    config.redis_url = nil
    config.redis_db = 'dyna_t'
    #config.redis_host = ENV['REDIS_HOST'] || 'localhost'
    config.redis_url = ENV['REDIS_URL']
    
    # Configure Mandrill
    #config.mandrill_key = ENV['MANDRILL_API_KEY'] || 'wtf'
  end
end
