require 'sidekiq/web'
Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  mount Sidekiq::Web => '/sidekiq'
  get '/:tree_id', to: 'dyna_t#cached_dyna_t'
  get '/:tree_id/parent/:id', to: 'dyna_t#parent'
  get '/:tree_id/child/:id', to: 'dyna_t#child'
end
