# Dyna-T

[![pipeline status](https://gitlab.com/arthurstomp/dynat/badges/master/pipeline.svg)](https://gitlab.com/arthurstomp/dynat/commits/master) [![coverage report](https://gitlab.com/arthurstomp/dynat/badges/master/coverage.svg)](https://gitlab.com/arthurstomp/dynat/commits/master)


## Dependencies

* Ruby 2.6.2
* Docker
* docker-compose

## Workflow - Transforming data

1. Fetcher performs an HTTP get to the link.
1. Fetcher calls for Persister.
1. Persister gets the response and persists it in `tmp/dyna_t/dyna_t.json`
(by default. To customize it see the configuration section).
1. Persister calls for ProcessDynaTJob.
1. ProcessDynaTJob goes through the tree and normalize it in the format:
```
{
id: {parents: [1,2,3], children: [{id: 1, child: []}]}
}
```
6. ProcessDynaTJob then calls for CacheHandler to persist the normilized tree
in a redis db.
6. Once the normalized tree is in Redis, it can be accessed through the API.
6. The endpoints of the API are served using DynaTree model that compose the 
anwser using data from Redis.

## API

* GET /:tree_id - Returns the cached tree.
* GET /:tree_id/parent/:id - Return the list of parent's id of node.
* GET /:tree_id/child/:id - Return the children of node.

## Configuration

### `.env`

For development, I recommend that you create a `.env` and load it using
`$ eval $(cat .env)`

```
# .env sample

export DYNA_T_SOURCE=<Your source for the tree>
export REDIS_HOST=redis
export REPORT_DEST=<Destination of report email>
export SENDGRID_API_KEY=<Your sendgrid api key>
```

## Running Locally

1. Load `.env` - `$ eval $(cat .env)`
1. Start Redis - `$ docker-compose up -d redis`
1. Start rails server - `$ bundle exec rails s`

## Testing

1. Load `.env` - `$ eval $(cat .env)`
1. Start Redis - `$ docker-compose up -d redis`
1. Run RSpec - `$ bundle exec rspec spec/`

## Deploying

This repo is using Gitlab's CI/CD tools. Pushes to master trigger the pipeline
that execute the tests and, if it's ok, gitlab perform the deploy on Heroku.
