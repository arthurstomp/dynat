require 'sendgrid-ruby'
require 'persister'

class Reporter
  include Singleton
  include SendGrid

  def send_report
    sendgrid.client.mail._('send').post(request_body: mail.to_json)
  end

  def mail
    mail = Mail.new(from, subject, to, content)
    mail
  end

  def content
    body = <<-EOF
      This is an email from Dyna-T application by Arthur Rocha de Menezes.

      Here you will find the last tree fetched from https://random-tree.herokuapp.com/

      You can operate over that tree here: https://dyna-t.herokuapp.com

      link to repository: https://gitlab.com/arthurstomp/dynat
      
      #{CacheHandler.instance.original}
    EOF

    Content.new(type: 'text/plain', value: body)
  end

  def subject
    'Arthur Rocha - Tree test'
  end

  def from
    Email.new email: 'arthurstomp@gmail.com'
  end

  def to
    Email.new email: ENV['REPORT_DEST']
  end

  def sendgrid
    @sendgrid ||= SendGrid::API.new(api_key: ENV['SENDGRID_API_KEY'])
  end
end
