# frozen_string_literal: true
require 'reporter'

namespace :dyna_t do
  desc 'Fetch, persist, process and cache tree.'
  task setup: :environment do
    SetupDynaTJob.perform_now
    puts '.'
  rescue => e
    puts e
    puts 'F'
  end

  desc 'Fetch, persist, process and cache tree.(async)'
  task setup_async: :environment do
    SetupDynaTJob.perform_later
    puts '.'
  rescue => e
    puts e
    puts 'F'
  end

  desc 'Send report email'
  task send_report: :environment do
    Reporter.instance.send_report
  end
end
