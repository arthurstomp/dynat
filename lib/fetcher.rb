class Fetcher
  include Singleton
  include HTTParty
  attr_reader :raw_data, :data
  def fetch!
    source = Rails.application.config.dyna_t_source
    @raw_data = worker.call(source).body
    CacheHandler.instance.original = @raw_data
    @data = JSON.parse(@raw_data)
  end

  def next!
    persister.persist!(data)
    persister.next!
  end

  private

  def persister
    Persister.instance
  end

  def worker
    injected_worker = Rails.application.config.dyna_t_fetch_worker
    @worker ||= injected_worker || default_worker
  end

  def default_worker
    lambda do |source|
      self.class.get(source)
    end
  end
end
