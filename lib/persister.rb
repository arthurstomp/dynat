# frozen_string_literal: true

# class Persister
#
class Persister
  include Singleton
  attr_reader :data
  attr_writer :dir, :filename

  def setup
    unless Dir.exist?(dir) && File.directory?(dir)
      Dir.mkdir(dir)
    else
      FileUtils.rm_rf(dir)
      Dir.mkdir(dir)
    end
  end

  def persist!(data)
    @data = data
    worker.call(file_path, data.to_json)
  end

  def next!
    ProcessDynaTJob.perform_now(data)
  end

  def file_path
    dir.join(filename)
  end

  def dir
    @dir ||= Rails.application.config.dyna_t_dir
  end

  def filename
    @filename ||= Rails.application.config.dyna_t_filename
  end

  def persisted_file
    File.read(file_path)
  end

  private

  def worker
    injected_worker = Rails.application.config.dyna_t_persist_worker
    @worker ||= injected_worker || local_persistence
  end

  def local_persistence
    lambda do |path, json|
      File.open(path, 'w') { |file| file.write(json) }
    end
  end
end
