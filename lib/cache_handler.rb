class CacheHandler
  include Singleton

  def get(key)
    redis_value = redis.get(key)
    if redis_value
      JSON.parse(redis_value)
    else
      nil
    end
  end

  def update(norm_tree)
    insert_normalized_tree(norm_tree)
  end

  def original=(new_tree)
    new_tree_json = new_tree.is_a?(String) ? new_tree : new_tree.to_json
    redis.set('original_tree', new_tree_json)
  end

  def original
    tree = redis.get('original_tree')
    if tree
      JSON.parse  tree
    end
  end

  def redis
    @redis ||= Redis.new(
      url: Rails.application.config.redis_url,
      host: Rails.application.config.redis_host,
      db: Rails.application.config.redis_db)
  end

  def insert_normalized_tree(norm_tree)
    redis.multi do
      norm_tree.each{ |k,v| redis.set(k, v.to_json) }
    end
  end
end
