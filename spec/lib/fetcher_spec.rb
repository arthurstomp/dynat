require 'rails_helper'
require 'persister'

RSpec.describe Fetcher do
  let(:subject) { Fetcher.instance }

  before :each do
    Rails.application.config.dyna_t_fetch_worker = nil
    get_res = double
    allow(get_res).to receive(:body) { little_tree_json }
    allow(described_class).to receive(:get) { get_res }
  end

  it 'returns the parsed JSON of fetched tree' do
    expect(subject.fetch!).to eq(little_tree)
    expect(subject.data).to eq(little_tree)
    expect(subject.raw_data).to eq(little_tree_json)
  end

  it 'calls persister' do
    subject.fetch!
    persister = Persister.instance
    expect(persister).to receive(:persist!).with(little_tree)
    expect(persister).to receive(:next!)
    subject.next!
  end
end
