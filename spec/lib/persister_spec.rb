require 'rails_helper'
require 'json'

RSpec.describe Persister do
  let(:subject) { Persister.instance }

  def clear_tmp_folder
    FileUtils.rm_rf(subject.dir)
  end

  before :each do
    subject.setup
  end

  it 'is a singleton' do
    expect(Persister.included_modules).to include(Singleton)
  end

  it 'has a dir' do
    expect(subject.dir).to eq Rails.root.join('spec', 'assets', 'dyna_t')
  end

  it 'has a filename' do
    expect(subject.filename).to eq 'dyna_t.json'
  end

  it 'setup dyna_t tmp dir' do
    clear_tmp_folder
    expect(Dir.exist?(subject.dir)).to eq false
    subject.setup
    expect(Dir.exist?(subject.dir)).to eq true
  end

  it 'persists json' do 
    json = JSON.generate({hello: :world})
    subject.persist!({hello: :world})
    expect(File.read(subject.file_path)).to eq json
  end

  it 'returns full file path' do
    expect(subject.file_path).to eq(subject.dir.join(subject.filename))
  end

  it 'returns the persisted file' do
    expect(File).to receive(:read) { 'hello' }
    expect(subject.persisted_file).to eq('hello')
  end

  it 'calls ProcessDynaTJob' do
    allow(subject).to receive(:data) { little_tree }
    expect(ProcessDynaTJob).to receive(:perform_now)
    subject.next!
  end
end
