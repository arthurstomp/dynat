require 'rails_helper'
require 'reporter'

RSpec.describe Reporter do
  let!(:subject) { described_class.instance }

  before :each do
    allow(CacheHandler.instance).to receive(:original) { {} }
  end

  it 'calls for mandrill' do
    sendgrid = subject.sendgrid
    allow(Persister.instance).to receive(:persisted_file) { 'hello' }
    client = double
    mail = double
    post = double
    expect(sendgrid).to receive(:client) { client }
    expect(client).to receive(:mail) { mail }
    expect(mail).to receive('_').with('send') { post }
    expect(post).to receive(:post)
    subject.send_report
  end
end
