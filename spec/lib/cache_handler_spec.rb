require 'rails_helper'

RSpec.describe CacheHandler do
  let(:subject) { described_class.instance }
  let!(:redis) { subject.redis }

  it 'returns value for key' do
    redis.set('hello', ['world'].to_json)
    expect(subject.get('hello')).to eq ['world']
  end

  context '#update' do
    it 'updates database structure' do
      redis.set('hello', ['world'])
      expect(redis.get('hello')).to eq ['world'].to_json
      subject.update({'hello' => 'hell'})
      expect(redis.get('hello')).to eq 'hell'.to_json
    end
  end

  it 'set the original tree' do
    expect(redis).to receive(:set)
    subject.original = {'id' => 1, 'child' => []}
  end

  it 'get the original tree' do
    expect(redis).to receive(:get).with('original_tree') { {}.to_json }
    subject.original
  end
end
