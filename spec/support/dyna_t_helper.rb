module DynaTHelper
  def little_tree_json
    File.read(Rails.root.join('spec', 'assets', 'little_tree.json'))
  end

  def little_tree
    JSON.parse(little_tree_json)
  end

  def real_tree_json
    File.read(Rails.root.join('spec', 'assets', 'real_tree.json'))
  end

  def real_tree
    JSON.parse(real_tree_json)
  end
end
