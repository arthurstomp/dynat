require 'rails_helper'

RSpec.describe SetupDynaTJob, type: :job do
  let(:subject) { described_class }

  it 'expect to call other libs and jobs' do
    expect(Fetcher.instance).to receive(:data) { {'id' => 1, 'child' => []} }
    expect(Fetcher.instance).to receive(:fetch!)
    subject.perform_now
  end
end
