require 'rails_helper'
require 'cache_handler'

RSpec.describe ProcessDynaTJob, type: :job do
  let!(:cache_handler) { CacheHandler.instance }
  let!(:redis) { cache_handler.redis }
  after :each do
    redis.call('FLUSHALL')
  end

  it 'update cache' do
    described_class.perform_now(little_tree)
    node = cache_handler.get(631.to_s)
    expect(node['parents'].count).to eq 2
    expect(node['children'].count).to eq 2
  end

  context 'little tree' do
    before :each do
      described_class.perform_now(little_tree)
    end

    it 'set node 9424 normalized' do
      node = cache_handler.get(9424.to_s)
      expect(node['parents'].count).to eq 1
      expect(node['parents'].first).to eq 1
      expect(node['children'].count).to eq 9
    end

    it 'set node 1192 normalized' do
      node = cache_handler.get(1192.to_s)
      expect(node['parents'].count).to eq 3
      expect(node['parents'].last).to eq 1
      expect(node['children'].count).to eq 0
    end
  end
end
