require 'rails_helper'

RSpec.describe 'DynaTController requests', type: :request do
  before :each do
    allow(DynaTree).to receive(:original_tree) { little_tree }
    ProcessDynaTJob.perform_now(little_tree)
  end

  it 'returns the cached dynamic tree' do
    get '/1'
    expect(response.body).to eq(little_tree_json)
  end

  it 'returns all parents of node in the dynamic tree'do
    get '/1/parent/631'

    expect(JSON.parse(response.body)).to eq [9424,1]
  end

  it 'returns all children of node in the dynamic tree' do
    get '/1/child/631'
    expected_children = [
      {"child"=>[], "id"=>4844},
      {"child"=>[], "id"=>8055}
    ]
    expect(JSON.parse(response.body)).to eq expected_children
  end
end
