require 'rails_helper'
require 'persister'

RSpec.describe DynaTree do
  let(:subject) { described_class }

  before :each do
    litte_tree_path = Rails.root.join('spec', 'assets', 'little_tree.json')
    CacheHandler.instance.original = little_tree
  end

  it 'returns the original json if tree_id matches' do
    expect(subject.tree(1.to_s)).to eq(little_tree)
    expect(subject.tree(2.to_s)).to eq nil
  end

  it 'returns a list of parents of node' do
    node_id = 631
    allow(CacheHandler.instance).to receive(:get).with(node_id.to_s) do
      {
        'children' => [],
        'parents' => [9424, 1]
      }
    end
    expect(subject.parents(1.to_s, node_id.to_s)).to eq [9424, 1]
    allow(CacheHandler.instance).to receive(:get).with(node_id.to_s) { nil }
    expect(subject.parents(1.to_s, node_id.to_s)).to eq []
  end
  it 'returns a list of parents of node' do
    node_id = 631
    expected_children = [
      {id: 2, child: []}, 
      {id: 3, child: []}
    ]
    allow(CacheHandler.instance).to receive(:get).with(node_id.to_s) do
      {
        'children' => expected_children,
        'parents' => []
      }
    end
    expect(subject.children(1.to_s, node_id.to_s)).to eq expected_children
    allow(CacheHandler.instance).to receive(:get).with(node_id.to_s) { nil }
    expect(subject.children(1.to_s, node_id.to_s)).to eq []
  end
end
