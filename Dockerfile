FROM ruby:2.6.1-alpine

RUN apk update && \
      apk add nodejs \
      libxml2-dev \
      libxslt-dev \
      build-base

RUN mkdir /app

WORKDIR /app

COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock

RUN gem install bundler && bundle install

COPY . /app
# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/entrypoint.sh
RUN chmod +x /usr/bin/entrypoint.sh
RUN bundle config build.nokogiri --use-system-libraries
ENTRYPOINT ["entrypoint.sh"]
ENV RAILS_ENV development
ENV REDIS_HOST redis
EXPOSE 3000

#RUN bundle exec sidekiq -d -L log/sidekiq.log
# Start the main process.
CMD ["rails", "server", "-b", "0.0.0.0"]
